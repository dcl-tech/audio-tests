# audio-tests demo scene

Demonstrates both an AudioStream (from https with CORS), and an AudioSource (local mp3 file)

Left/West side is stream, right/east side is file

Red button: stop audio
Green button: start audio
Small white button: play softly (initial state)
Large white button: play at a somewhat louder volume

the 'useSameEntity' boolean constant demonstrates that the same entity can support both an AudioStream and an AudioSource simultaneously playing.

