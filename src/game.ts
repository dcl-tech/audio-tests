/////////////////////////////////////////////////////////////////
// SDK6 example of stream and sounds playing, stopping, volume
// Carl Fravel
/////////////////////////////////////////////////////////////////

const useSameEntity = false
const STREAM_SOFT = 0.03
const STREAM_LOUD = .1
const SOUND_SOFT = 0.3
const SOUND_LOUD = 1

let mtlBlack = new Material()
mtlBlack.albedoColor=Color3.Black()

let mtlRed = new Material()
mtlRed.albedoColor=Color3.Red() 

let mtlGreen = new Material()
mtlGreen.albedoColor=Color3.Green()

////////////////////////////////////////////
////////////////// Stream //////////////////
////////////////////////////////////////////

let streamEntity = new Entity()
streamEntity.addComponent(new Transform({
  position:new Vector3(4,1,8),
  scale:new Vector3(1,1,1)
}
))
streamEntity.addComponent(new BoxShape())
streamEntity.addComponent(mtlBlack)
engine.addEntity(streamEntity)
// add Stream to entity
let audioStream = new AudioStream("https://api2.extraterra.com/audio-proxy/httpstream?http=http://uk7.internet-radio.com:8040")
// Set sound to soft, not playing
audioStream.volume = STREAM_SOFT
audioStream.playing = false
streamEntity.addComponent(audioStream)

let stopStreamEntity = new Entity()
stopStreamEntity.addComponent(new Transform({
  position:new Vector3(1,1,2),
  scale:new Vector3(0.5,0.5,0.5)
}
))
stopStreamEntity.addComponent(new BoxShape())
stopStreamEntity.addComponent(mtlRed)

engine.addEntity(stopStreamEntity) 
stopStreamEntity.addComponent(new OnPointerDown(()=>{
  audioStream.playing = false
}))

let playStreamEntity = new Entity()
playStreamEntity.addComponent(new Transform({
  position:new Vector3(2,1,2),
  scale:new Vector3(0.5,0.5,0.5)
}
))
playStreamEntity.addComponent(new BoxShape())
playStreamEntity.addComponent(mtlGreen)
engine.addEntity(playStreamEntity) 
playStreamEntity.addComponent(new OnPointerDown(()=>{
  audioStream.playing = true
}))

let streamSoftEntity = new Entity()
streamSoftEntity.addComponent(new Transform({
  position:new Vector3(3,1,2),
  scale:new Vector3(0.5,0.5,0.5)
}
))
streamSoftEntity.addComponent(new BoxShape())
engine.addEntity(streamSoftEntity) 
streamSoftEntity.addComponent(new OnPointerDown(()=>{
  audioStream.volume = STREAM_SOFT
}))

let streamLoudEntity = new Entity()
streamLoudEntity.addComponent(new Transform({
  position:new Vector3(5,1,2),
  scale:new Vector3(1,1,1)
}
))
streamLoudEntity.addComponent(new BoxShape())
engine.addEntity(streamLoudEntity) 
streamLoudEntity.addComponent(new OnPointerDown(()=>{
  audioStream.volume = STREAM_LOUD
}))


////////////////////////////////////////////
///////////////// SoundFile ////////////////
////////////////////////////////////////////

// Create entity
let soundEntity:Entity

// Create AudioClip object, holding audio file
const audioClip = new AudioClip("sounds/Hmm4.mp3")

// Create AudioSource component, referencing `clip`
const audioSource = new AudioSource(audioClip)

// Add AudioSource component to entity
if (useSameEntity)  {
  streamEntity.addComponent(audioSource)
}
else {
  let soundEntity:Entity = new Entity()
  soundEntity.addComponent(new Transform({
    position:new Vector3(12,1,8),
    scale:new Vector3(1,1,1)
  }))
  soundEntity.addComponent(new BoxShape())
  soundEntity.addComponent(mtlBlack)
  engine.addEntity(soundEntity)
  soundEntity.addComponent(audioSource)
}

// Set sound to looping, soft, not playing 
audioSource.loop = true
audioSource.volume = SOUND_SOFT
audioSource.playing = false

let stopSoundEntity = new Entity()
stopSoundEntity.addComponent(new Transform({
  position:new Vector3(9,1,2),
  scale:new Vector3(0.5,0.5,0.5)
}
))
stopSoundEntity.addComponent(new BoxShape())
stopSoundEntity.addComponent(mtlRed)

engine.addEntity(stopSoundEntity) 
stopSoundEntity.addComponent(new OnPointerDown(()=>{
  audioSource.playing = false
}))

let playSoundEntity = new Entity()
playSoundEntity.addComponent(new Transform({
  position:new Vector3(10,1,2),
  scale:new Vector3(0.5,0.5,0.5)
}
))
playSoundEntity.addComponent(new BoxShape())
playSoundEntity.addComponent(mtlGreen)
engine.addEntity(playSoundEntity) 
playSoundEntity.addComponent(new OnPointerDown(()=>{
  audioSource.playing = true
}))


let soundSoftEntity = new Entity()
soundSoftEntity.addComponent(new Transform({
  position:new Vector3(11,1,2),
  scale:new Vector3(0.5,0.5,0.5)
}
))
soundSoftEntity.addComponent(new BoxShape())
engine.addEntity(soundSoftEntity) 
soundSoftEntity.addComponent(new OnPointerDown(()=>{
  audioSource.volume = SOUND_SOFT
}))

let soundLoudEntity = new Entity()
soundLoudEntity.addComponent(new Transform({
  position:new Vector3(13,1,2),
  scale:new Vector3(1,1,1)
}
))
soundLoudEntity.addComponent(new BoxShape())
engine.addEntity(soundLoudEntity) 
soundLoudEntity.addComponent(new OnPointerDown(()=>{
  audioSource.volume = SOUND_LOUD
}))

